<?php

require_once __DIR__ . '/vendor/autoload.php';

use \Psr\Http\Message\{ServerRequestInterface as Request, ResponseInterface as Response};
use \coboard\control\Controleur as Controleur;


$c = new \Slim\Container(['settings' => ['displayErrorDetails' => true]]);
$app = new \Slim\App($c);

\wishlist\bd\Eloquent::start(__DIR__ . '/src/conf/conf.ini');

$app->get('/item/{id}', function (Request $request, Response $response, $args) {
    $c = new Controleur();
    $resp = $c->getItem($args['id']);
    $response->getBody()->write($resp);
    return $response;
});

$app->get('/liste/{no}', function (Request $request, Response $response, $args) {
    $c = new Controleur();
    $rep = $c->listeDeSouhait($args['no']);
    $response->getBody()->write($rep);
    return $response;
});

$app->get('/rendreListePublic', function (Request $request, Response $response, $args) {
	$c = new Controleur();
    $rep = $c->SelectionnerListe(3);
    $response->getBody()->write($rep);
    return $response;
});

$app->post('/rendrePublic', function (Request $request, Response $response, $args){
	$c = new Controleur();
    $rep = $c->RendrePublic($_POST['Code']);
	$response->getBody()->write($rep);
    return $response;
});

$app->get('/accueil', function (Request $request, Response $response, $args) {
    $c = new Controleur();
    $rep = $c->accueil();
    $response->getBody()->write($rep);
    return $response;
});

$app->get('/compte', function (Request $request, Response $response, $args) {
    $c = new Controleur();
    $rep = $c->infoCompte("test","test");
    $response->getBody()->write($rep);
    return $response;
});

$app->get('/creationListe', function (Request $request, Response $response, $args) {
    $c = new Controleur();
    $rep = $c->creationListe();
    $response->getBody()->write($rep);
    return $response;
});

$app->get('/formulaire', function (Request $request, Response $response, $args) {
    $c = new Controleur();
    $rep = $c->ajoutParticipant();
    $response->getBody()->write($rep);
    return $response;
});

$app->get('/inscription', function (Request $request, Response $response, $args) {
    $c = new Controleur();
    $rep = $c->inscription();
    $response->getBody()->write($rep);
    return $response;
});

$app->post('/creerListe', function (Request $request, Response $response, $args){
    $c = new Controleur();
    $rep = $c->newList($_POST['titre'], $_POST['descr'], $_POST['dateexp']);
    $response->getBody()->write($rep);
    return $response;
});

$app->post('/ajoutItemListe', function (Request $request, Response $response, $args){
	$c = new Controleur();
    $rep = $c->ajoutItemList($_POST['Code']);
	$response->getBody()->write($rep);
    return $response;
});

$app->post('/AjouterItemAListe/{id}', function (Request $request, Response $response, $args){
    $c = new Controleur();
    $rep = $c->newItemListe($_POST['Nom'], $_POST['Desc'], $_POST['Prix'], $_POST['Url'],$args['id']);
	$response->getBody()->write($rep);
    return $response;
});

$app->get('/ModifierListe', function (Request $request, Response $response, $args){
	$c = new Controleur();
    $rep = $c->SelectionnerListe(2);
	$response->getBody()->write($rep);
    return $response;
});

$app->get('/supprimerListe', function (Request $request, Response $response, $args){
    $c = new Controleur();
    $rep = $c->SuppListe();
    $response->getBody()->write($rep);
    return $response;
});

$app->post('/supprimerListe', function (Request $request, Response $response, $args){
    $c = new Controleur();
    $rep = $c->SupprimerListe($_POST['Code']);
    $response->getBody()->write($rep);
    return $response;
});

$app->post('/ChangerInfoListe/{id}', function (Request $request, Response $response, $args){
	$c = new Controleur();
    $rep = $c->ChangerInfo($_POST['Titre'], $_POST['Desc'], $_POST['Date'],$args['id']);
	$response->getBody()->write($rep);
    return $response;
});

$app->get('/SelectionnerListe', function (Request $request, Response $response, $args){
	$c = new Controleur();
    $rep = $c->SelectionnerListe(0);
	$response->getBody()->write($rep);
    return $response;
});

$app->post('/ModListe', function (Request $request, Response $response, $args){
	$c = new Controleur();
    $rep = $c->ModListe($_POST['Code']);
	$response->getBody()->write($rep);
    return $response;
});

$app->get('/connexion', function (Request $request, Response $response, $args) {
    $c = new Controleur();
    $rep = $c->connexion();
    $response->getBody()->write($rep);
    return $response;
});

$app->get('/PartagerListe', function (Request $request, Response $response, $args) {
    $c = new Controleur();
    $rep = $c->SelectionnerListe(1);
    $response->getBody()->write($rep);
    return $response;
});

$app->post('/Partage', function (Request $request, Response $response, $args){
	$c = new Controleur();
    $rep = $c->Partage($_POST['Code']);
	$response->getBody()->write($rep);
    return $response;
});

$app->post('/ajoutimage/{id}', function (Request $request, Response $response, $args){
    $c = new Controleur();
    echo '<a href="../item/';
    echo $args['id'];
    echo '">Retour<a/>';
    $rep = $c->ajoutimage($_POST['urlimage'],$args['id']);
    $response->getBody()->write($rep);
    return $response;
});


try {
    $app->run();
} catch (Throwable $e) {
    $e->getTraceAsString();
}



