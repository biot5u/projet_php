<?php

session_start();
if (isset($_POST['username']) && isset($_POST['password'])) {
    // connexion à la base de données
    $ini_array = parse_ini_file('src/conf/conf.ini');
    $dsn = 'mysql:host=localhost;dbname=pythoud2u';
    $db = new PDO($dsn, $ini_array['username'], $ini_array['password'], [
        PDO::ATTR_PERSISTENT => true,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_EMULATE_PREPARES => false,
        PDO::ATTR_STRINGIFY_FETCHES => false]);
    $db->prepare('SET NAMES \'UTF8\'')->execute();

    $username = $_POST['username'];
    $password = $_POST['password'];

    $p = "UPDATE MEMBRE set mdp = :password where utilisateur = :username";
    $req = $db->prepare($p);
    $req->execute(array('username' => $username, 'password' => $password));
    echo "Bonjour vous avez modifié votre mot de passe <a href=\"index.php/connexion\">Cliquez ici pour vous connecter à nouveau</a>";


}
