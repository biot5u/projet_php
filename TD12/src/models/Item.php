<?php

namespace coboard\models;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use coboard\vue\Participant;

/**
 * Post
 *
 * @mixin Builder
 */
class Item extends Model {

    private $reservation = 0;
    protected $table = 'item';
    protected $primaryKey = 'id';
    public $timestamps = false;

    function recupItem($id)
    {
        $item = Item::where('id', '=', $id)->first();
        return $item;
    }

    function itemsParListe($id){
        $list = Item::where('liste_id', '=', $id)->get();
        return $list;
    }

    function deleteItem($id) {
        $item = Item::where('id', '=', $id)->first();
        $item->delete();
    }
	
	function newItem($nom,$desc,$prix,$url,$id){
		$i = new Item();
		$i->liste_id = $id;
		$i->nom = $nom;
		$i->descr = $desc;
		$i->tarif = $prix;
		$i->url = $url;
		$i->save();
	}

	function reserverItem($id)
    {
        $item = Item::where('id', '=', $id)->first();

        if ($item->getReservation() == 0) {
            $item->setReservation();
            echo 'Item réservé';
        }
        else {
            echo 'Vous avez déjà reservé cette article';
        }
        return $item;
    }

    public function getReservation() {
        return $this->reservation;
    }

    public function setReservation() {
        $this->reservation = 1;
}

    public function modifImage($url, $id) {
        $item = Item::where('id', '=', $id)	->first();
        $item->img = $url;
        $item->save();
    }

		
}