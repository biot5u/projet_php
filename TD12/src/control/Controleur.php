<?php


namespace wishlist\control;

use wishlist\vue\{Compte, Connexion, Inscription, ItemView, ListView, PageView,Participant};
use wishlist\models\{Item, Liste, partageliste, publicliste};


class Controleur {

    function __construct()
    {

    }

    public function accueil() {
        $l = new Liste();
		$p = new publicliste();
		$p = $p->listeno();
        $res = $l->listerListes($p);		
        $listview = new PageView($res, "LIST_VIEW");
        return $listview->render();
    }

    public function connexion() {
        $connexion = new Connexion();
        return $connexion->render();
    }

    public function inscription() {
        $inscription = new Inscription();
        $inscription->register();
        return $inscription->render();
    }

    public function listeDeSouhait($id) {
        $i = new Item();
        $l = new Liste();
        $res = $i->itemsParListe($id);
        $res['nomliste'] = $l->titreParId($id);
        $v = new ItemView($res, "LIST_VIEW");
        return $v->render();
    }

    public function getItem($id) {
        $m = new Item();
        $item = $m->recupItem($id);
        $v = new ItemView($item, 'ITEM_VIEW');
        if ($item->getReservation() == 0)
            $item->setReservation();
        return $v->render();
    }

    public function ajoutParticipant() {
        $participant = new \wishlist\vue\Participant();
        return $participant->render();
    }

    public function newList($titre,$descr,$date){
        $l = new Liste();
        $res = $l->newList($titre, $descr, $date);
        $v = new PageView($res, 'RES_VIEW');
        return $v->render();
    }

    public function creationListe()
    {
        $res = '';
        $v = new PageView($res, 'FORM_VIEW');
        return $v->render();
    }

    public function ajoutItemList($id)
    {
        $l = new Liste();
        $l = $l->titreParId($id);
        if($l['no'] != null){
            $v = new PageView($l, 'AJOUTITEMLIST_VIEW');
            return $v->render();
        }
    }

    public function infoCompte($a,$b) {
        $c = new Compte($a,$b);
        return $c->render();
    }

    public function newItemListe($nom,$desc,$prix,$url,$id)
    {
        $i = new Item();
        $item = $i->newItem($nom,$desc,$prix,$url,$id);
    }


    public function ModifierListe()
    {
        $res = '';
        $v = new PageView($res, 'MOD_VIEW');
        return $v->render();
    }

    public function SuppListe()
    {
        $res = '';
        $v = new PageView($res, 'DELETELIST_VIEW');
        return $v->render();
    }

    public function SupprimerListe($token)
    {
        $res = new Liste();
        $res = $res->suppression($token);
        $v = new PageView($res, 'DELETELIST_VIEW');
        return $v->render();
    }

    public function SelectionnerListe($id)

    {
		if($id == 0){
			$res = '';
			$v = new PageView($res, 'SELECT_VIEW');
			return $v->render();
		}
		else if($id == 1){
			$res = '';
			$v = new PageView($res, 'SELECTPARTAGE_VIEW');
			return $v->render();
		}
		else if($id == 2){
			$res = '';
			$v = new PageView($res, 'MOD_VIEW');
			return $v->render();
		}
		else if($id == 3){
			$res = '';
			$v = new PageView($res, 'RENDREPUBLIC_VIEW');
			return $v->render();
		}
    }

    public function ChangerInfo($titre,$desc,$date,$id){
        $l = new Liste();
        $l = $l->ChangeInfo($titre,$desc,$date,$id);
    }

    public function ModListe($id)
    {
        $l = new Liste();
        $l = $l->titreParId($id);
        if($l['no'] != null){
            $v = new PageView($l, 'CHANGE_VIEW');
            return $v->render();
        }

    }

    public function Partage($code){
        $l = new Liste();
        $l = $l->titreParId($code);
        if($l['no'] != null){
            $p = new partageliste();
            $p = $p->newPartageListe($l['no'],$code);
            $v = new PageView($p, 'PARTAGE_VIEW');
            return $v->render();
        }

    }
	
	 public function RendrePublic($code){
        $l = new Liste();
        $l = $l->titreParId($code);
        $p = new publicliste();
		if($p->avoirliste($l->no)== null){
			$p = $p->newPublicListe($l->no);
		}

    }

    public function reserverItem($id) {
        $m = new Item();
        $i = $m->recupItem($id);
        $m = $m->reserverItem($id);
        $v = new ItemView($i, 'LIST_VIEW');
        return $v->render();
    }


    public function ajoutimage($url,$id) {
        $m = new Item();
        $i = $m->recupItem($id);
        $i->modifImage($url,$id);
    }

}