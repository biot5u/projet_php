<?php

namespace coboard\vue;

use wishlist\src\modele\Item;

class Participant
{

    private $utilisateur;

  function __construct()
  {
        session_start();
        $_SESSION['MaSession'] = $this->utilisateur;
  }


  public function render() {
      $html = <<< END
<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
          <!-- CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" href="../css/connexion.css">
        <!-- CSS -->

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:700&display=swap" rel="stylesheet">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    </head>
    <!-- head -->

    <!-- body -->
    <body>
    <!--header -->
        <div class="block">
            <header class="header">
                <a href="../index.html" class="header-logo">MyWishList</a>

                <nav class="header-menu">
                    <a href="../index.html">Accueil</a>
                    <a href="../index.php/accueil">Mes listes</a>
                    <a href="../index.php/creationListe">Créer Liste</a>
                    <a href="../index.php/supprimerListe">Supprimer Liste</a>
                    <a href="../index.php/SelectionnerListe">Ajouter Item</a>
                    <a href="../index.php/ModifierListe">Modifier Liste</a>
					<a href="../index.php/rendreListePublic">RendreListePublic</a>
                    <a href="../index.php/compte">Compte</a>
                    <a href="../index.php/connexion">Connexion</a>
                    <a href="../index.php/inscription">Inscription</a>
                </nav>
            </header>
        </div>
        
         <div class="banner">
           <div class="banner-content">
                <form id=reserverItem  method=get  action=../index.php/accueil>
                <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>
                <input type="password" placeholder="Entrez votre motivation à lé réserver" name="password" required>
                <p><input type=submit value='Confirmer votre réservation'</p>
              </form>
            </div>
        </div>
    </div>
    </body>
</html>
END;

        return $html;
  }
}