<?php


namespace coboard\vue;


class Connexion
{
    private $menu;

    function __construct () {
        $this->menu = new MenuView();
    }

    public function render() {
        $html = <<<END
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>MyWishList</title>

        <!-- CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" href="../css/inscription.css">
        <!-- CSS -->

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:700&display=swap" rel="stylesheet">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
        <!-- Font -->
    </head>
    <!-- head -->

    <!-- body -->
    <body>
    <!--header -->
        {$this->menu->ajoutMenu()}
        
        
            <!-- zone de connexion -->
        <div class="block">
          <div class="container">
             <form action="../verification.php" method="POST">'
                <h1>Connexion</h1>
                <label><b>Nom d'utilisateur</b></label>
                <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>
                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="password" required>
                <input type="submit" id='submit' value='LOGIN' >
            </form>
           </div>
        </div>
    </body>
</html>
END;

        return $html;
    }
}