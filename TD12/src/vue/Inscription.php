<?php


namespace coboard\vue;


class Inscription
{
    private $menu;
    private $compte;

    function __construct () {
        $this->menu = new MenuView();
        $this->compte = null;
    }

    public function register()
    {
        session_start();
        if (isset($_POST['username']) && isset($_POST['password'])) {
            // connexion à la base de données
            $db_username = 'root';
            $db_password = '';
            $db_name = 'test';
            $db_host = 'localhost';
            $db = mysqli_connect($db_host, $db_username, $db_password, $db_name)
            or die('could not connect to database');

            $username = mysqli_real_escape_string($db, htmlspecialchars($_POST['username']));
            $password = password_hash($_POST['password'], PASSWORD_DEFAULT);

            if ($username !== "" && $password !== "") {
                $requete = "INSERT INTO Membre set utilisateur = $username, mdp = $password";
                $exec_requete = mysqli_query($db, $requete);
            }
        }
    }

    public function render() {
        $html = <<<END
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>MyWishList</title>

        <!-- CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" href="../css/inscription.css">
        <!-- CSS -->

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:700&display=swap" rel="stylesheet">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>
        <!-- Font -->
    </head>
    <!-- head -->

    <!-- body -->
    <body>
    <!--header -->
    
        {$this->menu->ajoutMenu()}
        
        
            <!-- zone de connexion -->
        <div class="block">
          <div class="container">
             <form action="../register.php" method="POST">'
                <h1>Inscription</h1>
                <label><b>Nom d'utilisateur</b></label>
                <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>
                <label><b>Mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="password" required>
                <input type="submit" id='submit' value='LOGIN' >
            </form>
           </div>
        </div>
    </body>
</html>
END;

        return $html;
    }
}