<?php

namespace coboard\vue;

class PageView
{
    private $liste;
    private $selecteur;
    private $titre;
    private $menu;

    function __construct($l, $select)
    {

        $this->liste = $l;
        $this->selecteur = $select;
        $this->menu = new MenuView();
    }


    private function htmlList()
    {
        $content = "";
        $this->titre = "Listes";
        $content .= "<div class = content>
        <table>";
        foreach ($this->liste as $key => $value) {
            $no = $value['no'];
            $titre = $value['titre'];
            $desc = $value['description'];
            $content .= <<<EOF

            <tr>
                <td><a href="../index.php/liste/$no">$no</a></td>
                <td><a href="../index.php/liste/$no">$titre</a></td>
                <td><a href="../index.php/liste/$no">$desc</a></td>
            </tr>
</div>
EOF;
        }
        return $content . "
       </table>
    </div>
    <div class=button>
        <button type=submit onclick=\"window.location.href = '../index.php/creationListe'\">Creer liste</button>
    </div>";
    }

    private function formulaireAjout(){
        $this->titre = 'Créer une nouvelle liste';
        return   "
    <form id=ajoutListe  method=POST  action=../index.php/creerListe>
        <div>
          <label for= titre>Titre de la liste :</label>
          <input type = text name=titre >
        </div></br>
        <div>
          <label for= descr>Description :</label>
          <input type = text name=descr></div>
        <div></br>
          <label for= dateexp>Date d'expiration :</label>
          <input type = date name=dateexp>
        </div></br>
        <div class=\"button\">
          <input type=submit value='Creer liste'</input>
        </div>
    </form>";
    }
	
	private function AjoutItemListe($id){
		$this->titre = 'Ajouter un item a la liste';
		return  "<form action=../index.php/AjouterItemAListe/$id method=post>
	<p>Nom : <input type=text name=Nom /></p>
	<p>Description : <input type=text name=Desc /></p>
	<p>Prix : <input type=number name=Prix step=0.01 /></p>
	<p>URL : <input type=url name=Url /></p>
	<p><input type=submit value=OK></p>
	
	</form>";
	}
	
	private function SelectionnerListe(){
		$this->titre = 'Entrer le code de la liste à modifier';
		return  "<form action=../index.php/ajoutItemListe method=post>
	<p>Code : <input type=text name=Code /></p>
	<p><input type=submit value=OK></p>
	
	</form>";
	}

    private function SupprimerListe(){
        $this->titre = 'Entrer le code de la liste à supprimer';
        return  "<form action=../index.php/supprimerListe method=post>
	<p>Code : <input type=text name=Code /></p>
	<p><input type=submit value=OK></p>
	</form>";
    }
	
	private function CodeListe(){
		$this->titre = "Votre code d'accès a la liste est";
		return "<p>$this->liste</p>";
		
	}
	
	private function MyWishListe(){
		
	}
	
	private function ModifierListe($id){
		$this->titre = 'Entrer le code de la liste a modifier';
		return  "<form action=/projet_php/TD12/index.php/ChangerInfoListe/$id method=post>
	            <p>Titre : <input type=text name=Titre /></p>
	            <p>Description : <input type=text name=Desc /></p>
	            <p>Date d'expiration : <input type=date name=Date /></p>
	            <p><input type=submit value=OK></p>
	        </form>";
	}
	
	private function SelectListe(){
		$this->titre = 'Entrer le code de la liste a modifier';
		return  "<form action=../index.php/ModListe method=post>
	<p>Code : <input type=text name=Code /></p>
	<p><input type=submit value=OK></p>
	
	</form>";
	}
	
	private function SelectPartageListe(){
		$this->titre = 'Entrer le code de la liste a partager';
		return  "<form action=../index.php/Partage method=post>
	<p>Code : <input type=text name=Code /></p>
	<p><input type=submit value=OK></p>
	
	</form>";
		
	}
	
	private function Partage($id){
		$this->titre = 'Voici l URL que vous pouvez partager';
		return "https://webetu.iutnc.univ-lorraine.fr/www/pythoud2u/projet_php/TD12/index.php/liste/$id";
	}
	
	private function RendrePublicSelect(){
		$this->titre = 'Entrer le code de la liste a rendre public';
		return  "<form action=../index.php/rendrePublic method=post>
	<p>Code : <input type=text name=Code /></p>
	<p><input type=submit value=OK></p>
	
	</form>";
	}
	
	

    public function render()
    {
        global $content;
        switch ($this->selecteur) {
            case 'LIST_VIEW' :
            {
                $content = $this->htmlList();
                break;
            }
            case 'FORM_VIEW' :
            {
                $content = $this->formulaireAjout();
                break;
            }
			case 'RES_VIEW' :
			{
				$content = $this->CodeListe();
				break;
			}
			case 'AJOUTITEMLIST_VIEW' :
			{
				$content = $this->AjoutItemListe($this->liste['no']);
				break;
			}
			case 'MOD_VIEW' :
			{
				$content = $this->SelectListe();
				break;
			}
			case 'SELECT_VIEW' :
			{
				$content = $this->SelectionnerListe();
				break;
			}
			case 'CHANGE_VIEW' :
			{
				$content = $this->ModifierListe($this->liste['no']);
				break;
			}
			case 'SELECTPARTAGE_VIEW' :
			{
				$content = $this->SelectPartageListe();
				break;
			}
			case 'PARTAGE_VIEW' :
			{
				$content = $this->Partage($this->liste['no']);
				break;
				
			}

            case 'DELETELIST_VIEW' :
            {
                $content = $this->SupprimerListe();
                break;

            }

			case 'RENDREPUBLIC_VIEW' :
			{
				$content = $this->RendrePublicSelect();
				break;
			}

        }
        $html = <<<EOF
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>MyWishList</title>

        <!-- CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" href="../css/rubrique.css">
        <!-- CSS -->

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:700&display=swap" rel="stylesheet">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    </head>
    <!-- head -->

    <!-- body -->
    <body>
    <!--header -->
        

        {$this->menu->ajoutMenu()}
        
    <!--header -->
     <div class ="block">
            <h1 class="subtitle heading-site">$this->titre</h1>
     </div>
    $content
    </body>
</html>
EOF;

        return $html;
    }
}