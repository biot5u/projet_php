<?php


namespace coboard\vue;


use coboard\models\Item;

class ItemView
{
    private $liste;
    private $selecteur;
    private $titre;
    private $menu;

    function __construct ($l, $select) {
        $this->liste = $l;
        $this->selecteur = $select;
        $this->menu = new MenuView();
    }

    private function htmlListItem(){
        $this->titre = "Liste des items";
        $content = "<div class = content>
        <table>";
        foreach ($this->liste as $key => $value) {
            $id = $value['id'];
            $nom = $value['nom'];
            $desc = $value['descr'];
            $reservation = $value ['reservation'];
            $content .= <<<EOF
        
            <tr>
                <td><a href="../item/$id">$id</a></td>
                <td><a href="../item/$id">$nom</a></td>
                <td><a href="../item/$id">$desc</a></td></a>
                <td>Reservé : $reservation</td>
            </tr>
EOF;
        }
        return $content;
    }

    private function htmlUnItem(){
        $this->titre = "Un item";
        $nom = $this->liste['nom'];
        $desc = $this->liste['descr'];
        $id = $this->liste['id'];

        if (substr_compare($this->liste['img'],"http",0,5) == 1) {
            $img = $this->liste['img'];
        }   else {
            $img = '../../img/'.$this->liste['img'];
        }

        $content = "<div class = content>";
        $content .= <<< EOF
            <table>
                <td>$nom</td>
                <td>$desc</td>
                 <td><img src=$img alt =''/>
                 </br>
                 <form id=ajoutimage  method=POST  action=../../index.php/ajoutimage/$id>
                 <input type=submit value='Changer Image'/>
                 <input type=text name = "urlimage"/>
                 </br>
                 Soit un nom d'image inclus dans la base de donnee, soit une URL </br>
                 (Exemple, gouter.jpg ou https/www.miam.com/bongateau.jpg), et null </br>
                 pour supprimer l'image actuelle.
                 </form>
                 </td>
                 
                <form id=ajoutParticipant  method=GET  action=../formulaire>
                <input type=submit value='Réserver Item'/>
                </form>
            </table>
</div>
EOF;
        return $content;
    }

    public function render() {
        switch ($this->selecteur) {
            case 'LIST_VIEW' : {
                $content = $this->htmlListItem();
                break;
            }
            case 'ITEM_VIEW' : {
                $content = $this->htmlUnItem();
                break;
            }
        }
        $html = <<<END
<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
          <!-- CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">
        <link rel="stylesheet" href="../../css/rubrique.css">
        <!-- CSS -->

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:700&display=swap" rel="stylesheet">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    </head>
    <!-- head -->

    <!-- body -->
    <body>
    <!--header -->
        <div class="block">
            <header class="header">
                <a href="../../index.html" class="header-logo">MyWishList</a>

                <nav class="header-menu">
                    <a href="../../index.html">Accueil</a>
                    <a href="../../index.php/accueil">Mes listes</a>
                    <a href="../../index.php/creationListe">Créer Liste</a>
                    <a href="index.php/supprimerListe">Supprimer Liste</a>
                    <a href="../../index.php/SelectionnerListe">Ajouter Item</a>
					<a href="../index.php/rendreListePublic">RendreListePublic</a>
                    <a href="../../index.php/compte">Compte</a>
                    <a href="../../index.php/connexion">Connexion</a>
                    <a href="../../index.php/inscription">Inscription</a>
                    
                </nav>
            </header>
        </div>
        
    <!--header -->
     <div class ="block">
            <h1 class="subtitle heading-site">$this->titre</h1>
     </div>
    $content
    </body>
</html>
END;

        return $html;
    }
}