<?php


namespace coboard\vue;


class Compte
{

    private $utilisateur,$mdp,$menu;

    function __construct($u,$m)
    {
        $this->utilisateur = $u;
        $this->mdp = $m;
        $this->menu = new MenuView();
    }

    public function render() {
        $html = <<<EOF
<html>
    <head>
        <meta charset=\"UTF-8\">
        <title>MyWishList</title>

        <!-- CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css">

        <link rel="stylesheet" href="/projet_php/TD12/css/connexion.css">

        <link rel="stylesheet" href="../css/connexion.css">

        <!-- CSS -->

        <!-- Font -->
        <link href="https://fonts.googleapis.com/css?family=Montserrat:700&display=swap" rel="stylesheet">
        <script defer src="https://use.fontawesome.com/releases/v5.0.6/js/all.js"></script>

    </head>
    <!-- head -->

    <!-- body -->
    <body>
    <!--header -->
        

        {$this->menu->ajoutMenu()}
        
    <!--header -->
     
     <div class="banner">
           <div class="banner-content">
                <form id=modifierCompte  method=POST  action=../modifierCompte.php>
                <div>
               <label><b>Entrez votre nom d'utilisateur</b></label>
                <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>
                <label><b>Modifiez votre mot de passe</b></label>
                <input type="password" placeholder="Entrer le mot de passe" name="password" required>
          <p><input type=submit value='Modifier Compte'</p>
        </div></br>
        </form>
                 <form id=supprimerCompte  method=POST  action=../supprimerCompte.php>
                     <div>
                         <label><b>Entrez votre nom d'utilisateur</b></label>
                         <input type="text" placeholder="Entrer le nom d'utilisateur" name="username" required>
                         <label><b>Modifiez votre mot de passe</b></label>
                         <input type="password" placeholder="Entrer le mot de passe" name="password" required>
                         <p><input type=submit value='Supprimer Compte'</p>
                     </div></br>
                  </form>
             </div>
       </div>
     
    </body>
</html>
EOF;
        return $html;
    }
}