<?php

namespace coboard\vue;


class ListView
{
    private $liste;
    private $selecteur;
    private $titre;
    private $menu;

    function __construct($l, $select)
    {
        $this->liste = $l;
        $this->selecteur = $select;
        $this->menu = new MenuView();
    }

    private function htmlUneListe()
    {
        $this->titre = "Liste numéro " . $this->liste['no'];
        return $this->liste['titre'] . "<br/>" . $this->liste['description'];
    }

    public function render()
    {
        global $content;
        switch ($this->selecteur) {
            case 'SINGLE_VIEW' :
            {
                $content = $this->htmlUneListe();
                break;
            }
        }
        $html = <<<EOF
<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\">
        
    </head>
    <!-- head -->

    <!-- body -->
    <body>
    <!--header -->

        {$this->menu->ajoutMenu()}
        
    <!--header -->
     <div class ="block">
            <h1 class="subtitle heading-site">$this->titre</h1>
     </div>
    $content
    </body>
    
</html>
EOF;

        return $html;
    }
}